import queryString from 'query-string';
import { handleFakeData } from '@/fake-backend';
export const sendRequest = async (props) => {
    let {
        url,
        method,
        body,
        queryParams = null,
        useCredentials = false,
        auth = true,
        headers = {},
    } = props;

    if(auth) headers["Authorization"] = "Bearer " + JSON.parse(localStorage.getItem("ADSBASE-TOKEN"))?.token;

    const options = {
        method: method,
        // by default setting the content-type to be json type
        headers: new Headers({ 'Content-Type': 'application/json', ...headers }),
        body: body ? JSON.stringify(body) : null,
    };
    if (useCredentials) options.credentials = "include";

    if (queryParams) {
        url = `${url}?${queryString.stringify(queryParams)}`;
    }

    if(import.meta.env.VITE_FAKE_DATA == 'on') {
        return handleFakeData(url, options);
    }

    return fetch(url, options).then(res => {
        if (res.ok) {
            return res.json();
        } else {
            return res.json().then(function (json) {
                // to be able to access error status when you catch the error 
                if( json?.error?.token ) {
                    console.log('token expired');
                    localStorage.clear()
                    window.location.reload()
                }
                return {
                    statusCode: 'error',
                    message: JSON.stringify(json) ?? "",
                    error: json ?? ""
                };
            });
        }
    });
};


export const handleFetchAction = async(callback, state) => {
    state.status = 'spending'
    var result = {}
    try {
        result = await callback();
        if(result?.statusCode == 'error') {
            state.status = 'error'
            state.message = result?.message || 'Server error'
            console.error( result?.message )
            return
        }
        if(result.data) {
            state.status = 'success'
            state.data = result.data
            return
        }
    } catch (error) {
        state.status = 'error'
        state.message = result?.message || 'Server error'
        console.error( result?.message )
    }finally {
        return result
    }
}
