import { API_PROMPT, API_SEARCH_PROMPT } from "@/constants/api";
import { sendRequest } from "@/helper/api-method";
class PromptService {
    async getPromptDetails(id) {
        const url = API_PROMPT.replace(":id", id)
        const response = await sendRequest({
            url: url,
            method: "GET",
        })
        return response
    }

    async getPrompts(query) {
        const response = await sendRequest({
            url: API_SEARCH_PROMPT,
            method: "GET",
            queryParams: query
        })
        return response
    }
}

export default new PromptService ();