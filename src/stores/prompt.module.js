import { defineStore } from "pinia";
import PromptService from "@/services/Prompt.service";
export const usePromptStore = defineStore("prompt", {
    state: () => {
        return {
            promptId: '',
            searchPrompt: {
                status: '',
                result: [],
            },

            detailPrompt: {
                status: '',
                result: [],
            },
        };
    },

    actions: {
        setPromptId(id) {
            this.promptId = id;
        },

        async setSearchPromptsResult(query) {
            this.searchPrompt.status = 'spending';
            var result = {}
            try {
                result = await PromptService.getPrompts(query);
                if(result?.statusCode == 'error') {
                    this.searchPrompt.status = 'error';
                    console.error( result?.message )
                    this.searchPrompt.result = [];
                    return
                }
                if(result.data) {
                    this.searchPrompt.status = 'success';
                    this.searchPrompt.result = result.data;
                    return
                }
            } catch (error) {
                this.searchPrompt.status = 'error';
                console.error( result?.message )
                this.searchPrompt.result = [];
            }finally {
                this.searchPrompt.status = '';
            };
        },

        async setDetailPromptsResult(id) {
            this.detailPrompt.status = 'spending';
            var result = {}
            try {
                result = await PromptService.getPromptDetails(id);
                if(result?.statusCode == 'error') {
                    this.detailPrompt.status = 'error';
                    console.error( result?.message )
                    this.detailPrompt.result = [];
                    return
                }
                if(result.data) {
                    this.detailPrompt.status = 'success';
                    this.detailPrompt.result = result.data;
                    return
                }
            } catch (error) {
                this.detailPrompt.status = 'error';
                console.error( result?.message )
                this.detailPrompt.result = [];
            }finally {
                this.detailPrompt.status = '';
            }
        }
    }
});
