import { defineStore } from "pinia";

export const useRouteStore = defineStore("route", {
    state: () => {
        return {
            page: localStorage.getItem("page") || "login",
        };
    },
    actions: {
        setActivePage(value) {
            this.page = value
            localStorage.setItem("page", value)
        }
    },
});
