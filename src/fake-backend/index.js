export function handleFakeData(url, options) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (url.includes("/v1/categories") && url.includes("/prompts")) {
                const respone = {
                    data: [
                        {
                            id: 1,
                            name: "Create a Database",
                        },
                        {
                            id: 2,
                            name: "Create a Database Mongo",
                        },
                        {
                            id: 3,
                            name: "Create a Database MySQL",
                        },
                    ],
                };
                resolve(respone);
            } else if (url.includes("/v1/categories")) {
                const respone = {
                    data: {
                        mainCategories: [
                            {
                                id: 1,
                                name: "Health and Self",
                            },
                            {
                                id: 2,
                                name: "Coding",
                            },
                        ],
                        subCategories: {
                            1: [
                                {
                                    id: 3,
                                    name: "Exercise",
                                },
                                {
                                    id: 4,
                                    name: "Medical",
                                },
                            ],
                            2: [
                                {
                                    id: 5,
                                    name: "Database",
                                },
                            ],
                        },
                    },
                };
                resolve(respone);
            } else if (url.includes("/v1/prompts/")) {
                const respone = {
                    data: {
                        name: "Create a Database",
                        prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        bindings: [
                            {
                                type: "text",
                                name: "dbRelation",
                                placeholder:
                                    "Database tables and relationships",
                            },
                            {
                                type: "select",
                                name: "databaseType",
                                placeholder: "Select database query type",
                                options: [
                                    {
                                        name: "GraphQL",
                                        value: "GraphQL",
                                    },
                                    {
                                        name: "SQL",
                                        value: "SQL",
                                    },
                                ],
                            },
                        ],
                    },
                };
                resolve(respone);
            } else if (url.includes("/v1/prompts")) {
                const respone = {
                    data: [
                        {
                            id: 1,
                            name: "Create a Database",
                            prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        },
                        {
                            id: 2,
                            name: "Create a Database",
                            prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        },
                        {
                            id: 3,
                            name: "Create a Database",
                            prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        },
                        {
                            id: 8,
                            name: "Create a Database",
                            prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        },
                        {
                            id: 5,
                            name: "Create a Database",
                            prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        },
                    ],
                };
                resolve(respone);
            } else if (url.includes("/v1/ai-models")) {
                const respone = {
                    data: [
                        {
                            id: 123,
                            group: "AI content",
                            items: [
                                {
                                    id: 1,
                                    link: "https://chat.openai.com/",
                                    name: "Chat GPT",
                                    description:
                                        "Chat GPT là một trí tuệ nhân tạo có khả năng tương tác với con người qua văn bản. Nó được huấn luyện để hiểu và tạo ra ngôn ngữ tự nhiên, được sử dụng rộng rãi trong nhiều ứng dụng như hỗ trợ khách hàng, học tập trực tuyến và giải trí.",
                                    totalFavorite: 3,
                                    isFavorite: true, // trường này để xác định user đã yêu thích hay chưa
                                },
                            ],
                        },
                        {
                            id: 12,
                            group: "AI tạo ảnh",
                            items: [
                                {
                                    id: 1,
                                    link: "https://chat.openai.com/",
                                    name: "Chat GPT",
                                    description:
                                        "Chat GPT là một trí tuệ nhân tạo có khả năng tương tác với con người qua văn bản. Nó được huấn luyện để hiểu và tạo ra ngôn ngữ tự nhiên, được sử dụng rộng rãi trong nhiều ứng dụng như hỗ trợ khách hàng, học tập trực tuyến và giải trí.",
                                    totalFavorite: 3,
                                    isFavorite: true,
                                },
                                {
                                    id: 2,
                                    link: "https://chat.openai.com/",
                                    name: "Codeinum",
                                    description:
                                        "Codeinum là một trí tuệ nhân tạo có khả năng tương tác với con người qua văn bản. Nó được huấn luyện để hiểu và tạo ra ngôn ngữ tự nhiên, được sử dụng rộng rãi trong nhiều ứng dụng như hỗ trợ khách hàng, học tập trực tuyến và giải trí.",
                                    totalFavorite: 55,
                                    isFavorite: false,
                                },
                            ],
                        },
                    ],
                };
                resolve(respone);
            } else if (
                url.includes("/v1/categories") &&
                url.includes("/prompts")
            ) {
                const respone = {
                    data: [
                        {
                            id: 1,
                            name: "Create a Database",
                        },
                        {
                            id: 2,
                            name: "Create a Database Mongo",
                        },
                        {
                            id: 3,
                            name: "Create a Database MySQL",
                        },
                    ],
                };
                resolve(respone);
            } else if (url.includes("/v1/categories")) {
                const respone = {
                    data: {
                        mainCategories: [
                            {
                                id: 1,
                                name: "Health and Self",
                            },
                            {
                                id: 2,
                                name: "Coding",
                            },
                        ],
                        subCategories: {
                            1: [
                                {
                                    id: 3,
                                    name: "Exercise",
                                },
                                {
                                    id: 4,
                                    name: "Medical",
                                },
                            ],
                            2: [
                                {
                                    id: 5,
                                    name: "Database",
                                },
                            ],
                        },
                    },
                };
                resolve(respone);
            } else if (url.includes("/v1/prompts/")) {
                const respone = {
                    data: {
                        name: "Create a Database",
                        prompt: "Act as a database developer.  Respond to me with [[databaseType]].  I will explain what I want you to build and you will create the database schema for me.  You will also build the relationships for the instructions. Please create [[dbRelation]]",
                        bindings: [
                            {
                                type: "text",
                                name: "dbRelation",
                                placeholder:
                                    "Database tables and relationships",
                            },
                            {
                                type: "select",
                                name: "databaseType",
                                placeholder: "Select database query type",
                                options: [
                                    {
                                        name: "GraphQL",
                                        value: "GraphQL",
                                    },
                                    {
                                        name: "SQL",
                                        value: "SQL",
                                    },
                                ],
                            },
                        ],
                    },
                };
                resolve(respone);
            } else if (url.includes("/auth/login")){
                const respone = {
                    data: {
                        access_token: "access_token",
                    }
                };
                resolve(respone);
            } else if (url.includes("/api/v1/auth/firebase")) {
                const respone = {
                    data: {
                        access_token: "access_token",
                    }
                };
                resolve(respone);
            }
            else if (url.includes("/v1/ai-models/favorites")) {
                console.log('favorites')
                const respone = {
                    data:[]
                };
                resolve(respone);
            }
            resolve(null);
        }, 1000);
    });
}
