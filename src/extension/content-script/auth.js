
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if(request.checkIsLogin){
        checkToken()
    }
});

function isEmpty(value) {
    if (value === null || value === undefined) {
        return true;
    }
    if (typeof value === "string" && value.trim() === "") {
        return true;
    }
    if (Array.isArray(value) && value.length === 0) {
        return true;
    }
    if (
        typeof value === "object" &&
        Object.keys(value).length === 0 &&
        value.constructor === Object
    ) {
        return true;
    }  
    return false;
}


function checkToken() {
    const interval = setInterval(function () {
        const token = JSON.parse(localStorage.getItem('ADSBASE-TOKEN'))
        if (!isEmpty(token)) {
            chrome.runtime.sendMessage({ token: token, });
            clearInterval(interval);
            return
        }
    }, 1000);
}


checkToken()
