// Import the functions you need from the SDKs you need
import {initializeApp} from "firebase/app";
import 'firebase/auth'
import {
  VITE_APP_APIKEY,
  VITE_APP_AUTHDOMAIN,
  VITE_APP_PROJECTID,
  VITE_APP_STORAGEBUCKET,
  VITE_APP_MESSAGEINGSENDERID,
  VITE_APP_APPID,
  VITE_APP_MEASUREMENTID
} from "./firebase"
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
  apiKey: VITE_APP_APIKEY,
  authDomain: VITE_APP_AUTHDOMAIN,
  projectId: VITE_APP_PROJECTID,
  storageBucket: VITE_APP_STORAGEBUCKET,
  messagingSenderId: VITE_APP_MESSAGEINGSENDERID,
  appId: VITE_APP_APPID,
  measurementId: VITE_APP_MEASUREMENTID
};


const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
export const getFireBase = () => {
  return app;
}
