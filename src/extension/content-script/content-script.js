
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    // console.log("content received", request);
    // console.log(request)
    if(request.message) {
        handleGpt(request.message)
        handleGemini(request.message)
    }
});

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    // console.log("content received click", request);
    // Tạo một sự kiện input change
    if (request.submitAction) {
        handleButtonGpt();
        handleButtonGemini()
    }

});

chrome.runtime.sendMessage({ url: window.location.href });
//gpt

function handleGpt(message){
    const textarea = document.querySelector("#prompt-textarea");
    if(!textarea) return;
    textarea.value = message;
    // Tạo một sự kiện input change
    const inputChangeEvent = new Event("input", {
        bubbles: true, // Cho phép sự kiện lan truyền lên các phần tử cha
        cancelable: true, // Cho phép sự kiện có thể bị hủy bỏ
    });

    // Bắn sự kiện input change vào phần tử input
    textarea.dispatchEvent(inputChangeEvent);
}

function handleButtonGpt(){
    // const submitButtonGtp = document.querySelector(
    //     '[data-testid="send-button"]'
    // );
    // if(!submitButtonGtp) return
    // submitButtonGtp.click()

    const textarea = document.querySelector("#prompt-textarea");
    if(!textarea) return;
    const enterEvent = new KeyboardEvent('keydown', {
        key: 'Enter',
        keyCode: 13,
        code: 'Enter',
        which: 13,
        bubbles: true,
        cancelable: true
    });

    // Bắn sự kiện input change vào phần tử input
    textarea.dispatchEvent(enterEvent);
}


//gemini
function handleGemini(message){
    const textarea = document.querySelector("rich-textarea")
    // console.log(textarea)
    if(!textarea) return
    const content = textarea.querySelector("p")
    content.innerText = message

    const inputChangeEvent = new Event("input", {
        bubbles: true, // Cho phép sự kiện lan truyền lên các phần tử cha
        cancelable: true, // Cho phép sự kiện có thể bị hủy bỏ
    });

    // Bắn sự kiện input change vào phần tử input
    textarea.dispatchEvent(inputChangeEvent);
}

function handleButtonGemini(){
    const submitButtonGemini = document.querySelector(
        ".send-button"
    );
    if(!submitButtonGemini) return
    submitButtonGemini.click()
}

