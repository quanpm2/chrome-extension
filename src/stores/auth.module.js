import { defineStore } from "pinia";
import AuthenticationService from "@/services/Authentication.service";

export const useAuthStore = defineStore("auth", {
    state: () => {
        return {
            auth: {
                token: localStorage.getItem("token") || "",
                status: '',
                message: ''
            },
            register: {
                status: '',
                message: '',
                data: {}
            },

            authGoogle: {
                status: '',
                message: '',
                data: localStorage.getItem('token') || '',
            }
        };
    },
    // could also be defined as
    // state: () => ({ count: 0 })
    actions: {

        setStatus(status, message = '') {
            this.auth.message = message
            this.auth.status = status
        },

        setAuthToken(token) {
            
            this.auth.token = token;
            this.authGoogle.data = token
            localStorage.setItem("token", token);
        },
        async setLogin(payload) {
            this.setStatus('spending')
            var result = {}
            try {
                result = await AuthenticationService.login(payload);
                if(result?.statusCode == 'error') {
                    if(result?.message.includes('account')) {
                        this.setStatus('error', 'Tên đăn nhập hoặc mật khẩu không hợp lệ!')
                    } else{
                        this.setStatus('error', result?.message)
                    }
                    console.error( result?.message )
                    return
                }
                if(result.data) {
                    this.setStatus('success')
                    this.setAuthToken(result.data.access_token)
                    return
                }
            } catch (error) {
                console.error( result?.message )
                this.setStatus('error', result?.message)
            }finally {
            }
        },

        async setLoginGoogle() {
            this.authGoogle.status = 'spending'
            var result = {}
            try {
                result = await AuthenticationService.loginGoogle();
                if(result?.statusCode == 'error') {
                    this.authGoogle.status = 'error'
                    this.authGoogle.message = 'Có lỗi xảy ra trong quá trình đăng nhập!'
                    console.error( result?.message )
                    return
                }
                if(result.data) {
                    this.authGoogle.status = 'success'
                    this.authGoogle.data = result.data.access_token
                    localStorage.setItem("token", result.data.access_token)
                    return
                }
            } catch (error) {
                console.error( result?.message )
                this.authGoogle.status = 'error'
                this.authGoogle.message = 'Có lỗi xảy ra trong quá trình đăng nhập!'
            }finally {
            }
        }

        
    },
});
