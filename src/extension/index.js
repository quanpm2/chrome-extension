export function sendCount(count) {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { greeting: count });
    });
}

export function sendPromptResult(message) {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { message });
    });
}

export function sendSubmitAction() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { submitAction: true });
    });
}

export function loginGoolge() {

    const urlToOpen = "https://adsbase.adstech.asia";

    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        const currentTab = tabs[0];
        if (currentTab.url.includes(urlToOpen)) {
            console.log("Already logged in");
        } else {
            chrome.tabs.create({ url: urlToOpen });
        }
    });
}

export function checkLogin() {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { checkIsLogin: true });
    });
}

// auth

chrome.runtime?.onMessage.addListener((request, sender, sendResponse) => {
    // console.log("content received click", request);
    // Tạo một sự kiện input change
    if (request.token) {
        localStorage.setItem("ADSBASE-TOKEN", JSON.stringify(request.token));
        localStorage.setItem("page", "extension");
        window.location.reload();
    }
});

function verifyUrl(urlArray, url) {
    return urlArray.some((urlItem) => url.includes(urlItem));
}

chrome.runtime?.onMessage.addListener(function (message, sender, sendResponse) {
    console.log("extension received", message);

    if (message.url) {
        const locationPermessions = [
            "https://chatgpt.com",
            "https://gemini.google.com",
        ];

        const button = document.querySelector("#contentButton");
        if (!verifyUrl(locationPermessions, message.url)) {
            button.style.display = "none";
            if (localStorage.getItem("activeSidebar") == "Prompt") {
                localStorage.setItem("activeSidebar", "ListToolAIVue");
                window.location.reload();
            }
            return;
        }
        button.style.display = "flex";
        console.log("extension received url", message.url);
    }
});
