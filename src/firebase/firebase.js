export const  VITE_APP_APIKEY = import.meta.env.VITE_APP_APIKEY
export const  VITE_APP_AUTHDOMAIN = import.meta.env.VITE_APP_AUTHDOMAIN
export const  VITE_APP_PROJECTID = import.meta.env.VITE_APP_PROJECTID
export const  VITE_APP_STORAGEBUCKET = import.meta.env.VITE_APP_STORAGEBUCKET
export const  VITE_APP_MESSAGEINGSENDERID = import.meta.env.VITE_APP_MESSAGEINGSENDERID
export const  VITE_APP_APPID = import.meta.env.VITE_APP_APPID
export const  VITE_APP_MEASUREMENTID = import.meta.env.VITE_APP_MEASUREMENTID
