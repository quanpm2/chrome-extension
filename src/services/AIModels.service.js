import { AIRepositoryAPI, AIFavoriteAPI, API_FAVORITE_LIKE } from "../constants/api";
import {sendRequest} from "../helper/api-method"
class AIRepository {
    async getAIServices() {
        const response = await sendRequest({
            url: AIRepositoryAPI,
        })

        return response;
    }

    async getAIWhitelist() {
        const response = await sendRequest({
            url: AIFavoriteAPI,
        })

        return response;
    }

    async addAiToFavorite(id) {
        const url = API_FAVORITE_LIKE.replace(":id", id)
        const response = await sendRequest({
            url: url,
            method: "POST",
        })
        return response;
    }
}

const AIRepositoryClass = new AIRepository();

export default AIRepositoryClass;