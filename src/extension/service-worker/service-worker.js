console.log("service-worker loaded");

chrome.tabs.onUpdated.addListener(async (tabId, info, tab) => {
    if (!tab.url) return;
    const url = new URL(tab.url);
    // Enables the side panel on google.com
    if (url.origin) {
        await chrome.sidePanel.setOptions({
            tabId,
            path: "index.html",
            enabled: true,
        });
    } else {
        // Disables the side panel on all other sites
        await chrome.sidePanel.setOptions({
            tabId,
            enabled: false,
        });
    }
});

chrome.sidePanel
    .setPanelBehavior({ openPanelOnActionClick: true })
    .catch((error) => console.error(error));

chrome.runtime.onInstalled.addListener(() => {
    chrome.contextMenus.create({
        id: "openSidePanel",
        title: "Open side panel",
        contexts: ["all"],
    });
});

chrome.tabs.onActivated.addListener(function (activeInfo) {
    chrome.tabs.get(activeInfo.tabId, function (tab) {
        // console.log("Tab URL:", tab.url);
        sendMessageToPopup(tab.url);
    });
});

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (tab.active && changeInfo.url) {
        // console.log("Updated Tab URL:", changeInfo.url);
        sendMessageToPopup(changeInfo.url);
    }
});

function sendMessageToPopup(url) {
    chrome.runtime.sendMessage({ url: url }, function (response) {
        console.log("Message sent to popup with URL:", url);
    });
}
