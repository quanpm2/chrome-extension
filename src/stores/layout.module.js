import { defineStore } from 'pinia'
import { SIDE_BAR } from "@/constants/enums/layout"

export const useLayoutStore = defineStore('layout', {
  state: () => {
    return { 
      activeSideBar: localStorage.getItem("activeSidebar") || SIDE_BAR.ListTool,
      historyRoute: {
        before: localStorage.getItem("beforeRoute") || SIDE_BAR.ListTool,
        current: localStorage.getItem("currentRoute") || SIDE_BAR.ListTool
      }
    }
  },
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    setActiveSidebar(item, clearDetailAI = true) {
      this.historyRoute.before = this.activeSideBar
      localStorage.setItem('beforeRoute', this.activeSideBar)

      localStorage.setItem('activeSidebar', item)
      this.activeSideBar = item
      
      this.historyRoute.current = this.activeSideBar
      localStorage.setItem('currentRoute', this.activeSideBar)

      if(clearDetailAI)
      {
        if(localStorage.getItem("detailAi"))
        {
            localStorage.removeItem("detailAi")
        }
      }
    },
  },
})