import { API_LOGIN, API_REGISTER, API_LOGIN_FIREBASE } from "@/constants/api";
import { sendRequest } from "@/helper/api-method";
import { getFireBase } from "@/firebase";
import { GoogleAuthProvider, getAuth, signInWithPopup } from "firebase/auth";
class Auth {
    async login(payload) {
        const url = API_LOGIN
        const response = await sendRequest({
            url: url,
            method: "POST",
            body: payload
        })
        return response
    }
    async register(payload) {
        const url = API_REGISTER
        const response = await sendRequest({
            url: url,
            method: "POST",
            body: payload
        })
        return response
    }

    async loginGoogle() {
        try {  
            getFireBase();
            const provider = new GoogleAuthProvider()
            const auth = getAuth();
            const result = await signInWithPopup(auth,provider)
            const credential = GoogleAuthProvider.credentialFromResult(result);
            const feedback = await sendRequest({
                url: API_LOGIN_FIREBASE,
                method: "POST",
                body: {
                    idToken: credential.idToken
                }
            })
            return feedback
        } catch (error) {
            return error
        }
    }
}

export default new Auth ();