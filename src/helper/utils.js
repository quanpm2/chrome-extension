export const checkEmpty = (value) => {
    if (value === undefined || value === null) {
        return true; // Trả về true nếu giá trị là undefined hoặc null
    }

    if (typeof value === 'string' && value.trim() === '') {
        return true; // Trả về true nếu giá trị là một chuỗi rỗng hoặc chỉ chứa khoảng trắng
    }

    if (Array.isArray(value) && value.length === 0) {
        return true; // Trả về true nếu giá trị là một mảng rỗng
    }

    if (typeof value === 'object' && Object.keys(value).length === 0) {
        return true; // Trả về true nếu giá trị là một object rỗng
    }

    return false; // Trả về false trong các trường hợp còn lại
}