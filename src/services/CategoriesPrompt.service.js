import { API_CATEGORIES, API_CATEGORIES_PROMPTS } from "@/constants/api";
import { sendRequest } from "@/helper/api-method";
class CategoriesPromptService {
    async getCategories() {
        const response = await sendRequest({
            url: API_CATEGORIES,
            method: "GET",
        })
        return response
    }

    async getPromptsByCategory(id) {
        const url = API_CATEGORIES_PROMPTS.replace(":id", id)
        const response = await sendRequest({
            url: url,
            method: "GET",
        })
        return response
    }
}

export default new CategoriesPromptService();