import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import PrimeVue from 'primevue/config';
import 'primevue/resources/themes/aura-light-green/theme.css'
import 'primeicons/primeicons.css'
import { createPinia } from 'pinia'
// import { createGtm } from '@gtm-support/vue-gtm';
import Tooltip from 'primevue/tooltip';
import ToastService from 'primevue/toastservice';
import Ripple from 'primevue/ripple';
const pinia = createPinia()
const app = createApp(App)
app.use(pinia)
app.use(PrimeVue, {ripple: true})
app.directive('tooltip', Tooltip)
app.use(ToastService)
app.directive('ripple', Ripple)
// app.use(createGtm({ id: 'GTM-NNVMVG9B', }))
app.mount('#app')
